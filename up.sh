#! /bin/bash

PWD=`pwd`

EMACS_FILE=~/.emacs
EMACS_DIR=~/.emacs.d

cp -R $EMACS_FILE $PWD/.
cp -R $EMACS_DIR  $PWD/.
